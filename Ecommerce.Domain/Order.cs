﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Entities
{
    public class Order
    {
        public int OrderID { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateConfirmed { get; set; }
        public virtual ICollection<OrderLine> OrderLines { get; set; }
        public virtual User Buyer { get; set; }
        public Order()
        {

        }
    }
}
