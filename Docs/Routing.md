#Guía para clase con routing

+ Creamos un Web Site en visual studio
```Archivo>Agregar>Nuevo Sitio Web```

+ Generamos el archivo index.html
3. Agregamos los scripts de angularJS y Bootstrap desde Scripts/lib
4. Generamos barra navegadora de la aplicación, que tenga las siguientes opciones: Home, Productos, Compras

```html

<!DOCTYPE html><html ng-app="ecommerceApp"><head>    <title>Ecommerce APP</title>    <script src="//code.angularjs.org/1.4.0/angular.js"></script>    <link href="/Content/bootstrap.min.css" rel="stylesheet" />    <script src="/Scripts/lib/jquery-1.11.3.min.js"></script>    <script src="/Scripts/lib/bootstrap.min.js"></script>    <script src="/Scripts/app.js"></script>
</head><body>    <header>            <nav class="navbar navbar-default">            <div class="container">                <div class="navbar-header">                    <a class="navbar-brand" href="/">Ecommerce</a>                </div>                <ul class="nav navbar-nav navbar-right">                    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>                    <li><a href="#"><i class="fa fa-home"></i> Productos</a></li>                    <li><a href="#"><i class="fa fa-home"></i> Compras</a></li>                </ul>            </div>            </nav>        </header></body></html>

```

+ Creamos app.js

``` 
(function () {    'use strict';    var app = angular.module('EcommerceApp', []);})();
```

+ Agregamos referencia a ngRoute

``` javascript
//en app.js
var app = angular.module('ecommerceApp', ['ngRoute']);

//en index.html
<script src="https://code.angularjs.org/1.4.6/angular-route.min.js"></script>

```

+ Configuramos las rutas para home, articulos y compras en app.js

```javascript
var app = angular.module('ecommerceApp', ['ngRoute']);

app.config(function ($routeProvider) {        $routeProvider        .when('/', {            templateUrl: 'pages/home.html',            controller: 'homeController'        })        .when('/products', {            templateUrl: 'pages/products.html',            controller: 'productsController'        })        .when('/orders', {            templateUrl: 'pages/orders.html',            controller: 'ordersController'        })    });
```

+ Creamos carpeta Scripts/Controllers, y en ella creamos UsersController.js, ProductsController.js, OrdersControllers.js y HomeController.js

![](images/controllers.png)

En cada controller agregamos lo siguiente, variando para cada controller su nombre y el tipo correspondiente.

```javascript
(function () {    'use strict';    angular        .module('ecommerceApp')        .controller('productsController', function () {        console.log("products");    });})();

```

+ Creamos carpeta pages, y en ella creamos home.html, products.html, orders.html
![](images/pages.png)
En cada archivo agregamos el siguiente código, cambiando el texto que se muestra según el nombre del archivo.

```html
<div class="container">    <h1>Home</h1></div>
```

Al correr la aplicación, tendremos una aplicación web con navegabilidad. Es interesante inspeccionar el elemento de la vista, para detectar qué pasa cuando cambia  el ng-view.

+ Cambiamos el controller de productos. 
Le agregamos los servicios $scope para mantener la lista de productos, $http para obtener los productos desde la api, y $log para mostrar los resultados por la consola

```javascript
(function () {    'use strict';    angular        .module('ecommerceApp')        .controller('productsController', function ($scope, $http, $log) {                });})();
```

Le agregamos los servicios $scope para mantener la lista de productos, $http para obtener los productos desde la api, y $log para mostrar los resultados por la consola

```javascript
(function () {    'use strict';    angular        .module('ecommerceApp')        .controller('productsController', function ($scope, $http, $log) {
         $http.get('http://localhost:2022/api/products')        .success(function (result) {            $scope.products = result;        })            .error(function (data, status) {                $log.error(data);            });                });})();
```

De esta manera ya tenemos los productos para mostrar, ahora debemos modificar el html para que muestre lo que queremos.

+En product.html
Dado que el routing nos dicta el controller que estaremos utilizando, no es necesario escribirlo aquí. En primer lugar armemos el html para un objeto ```product```

```html
<div class="container">        <div class="row">            <div class="col-lg-4 col-sm-6 text-center">                <img class="img-circle img-responsive img-center" 
                src="{{ product.Images[0].ImageLink }}" alt="">                <h3>{{ product.Name }}                    <small>{{ product.Price }}</small>                </h3>                <p>{{ product.Description }}</p>            </div>        </div></div>
```
+Como queremos que se repita, agregamos la directiva ng-repeat

```html
<div class="container">    <div ng-repeat="product in products">        <div class="row">            <div class="col-lg-4 col-sm-6 text-center">                <img class="img-circle img-responsive img-center" 
                src="{{ product.Images[0].ImageLink }}" alt="">                <h3>{{ product.Name }}                    <small>{{ product.Price }}</small>                </h3>                <p>{{ product.Description }}</p>            </div>        </div>    </div></div>
```

##Ejercicio:
Replicar lo realizado en los productos para las compras.