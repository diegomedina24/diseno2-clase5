(function () {
    'use strict';
    var app = angular.module('ecommerceApp', ['ngRoute']);

    app.config(function ($routeProvider) {

        $routeProvider

        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'homeController'
        })

        .when('/products', {
            templateUrl: 'pages/products.html',
            controller: 'productsController'
        })

        .when('/orders', {
            templateUrl: 'pages/orders.html',
            controller: 'ordersController' 
        })

        .otherwise({
            redirectTo: '/'
        })
    });
})();